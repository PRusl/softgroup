<?php
/*
Plugin Name: Twitters widget
Plugin URI: http://
Description: Добавляє за допомогою крона твіти в базу та виводить їх за допомогою віджета.
Version: 1.0
Author: Ruslan
Author URI: http://
*/
/*  Copyright 2017  Ruslan  (email: E-MAIL_АВТОРА)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

add_action('plugins_loaded', 'myplugin_init');
function myplugin_init(){
    load_plugin_textdomain('wtw');
}

add_action('admin_menu', function () {
    add_menu_page('twitter widget', 'Твіттер віджет', 'manage_options', __FILE__);
});
add_action('admin_menu', function () {
    add_submenu_page(__FILE__, 'Твіттер віджет', 'Налаштування', 'manage_options', __FILE__, 'twitter_submenu');
});
function twitter_submenu()
{
    echo '<h1>Settings for Twitters API</h1>';

    if (empty(get_option('tw'))) {
        $fields = [
            [
                'label' => 'twitter User (radiosvoboda)',
                'name' => 'twitter_user',
                'value' => null,
            ],
            [
                'label' => 'oauth_access_token',
                'name' => 'oauth_access_token',
                'value' => null
            ],
            [
                'label' => 'oauth_access_token_secret',
                'name' => 'oauth_access_token_secret',
                'value' => null,
            ],
            [
                'label' => 'consumer_key',
                'name' => 'consumer_key',
                'value' => null,
            ],
            [
                'label' => 'consumer_secret',
                'name' => 'consumer_secret',
                'value' => null,
            ],

        ];

        update_option('tw', $fields);
    }

    $fields = get_option('tw');

    echo '<form class="form" method="post" enctype="multipart/form-data">';
    foreach ($fields as $field) {
        echo '
        <label for="' . $field["name"] . '">' . $field["label"] . '</label>
        <input type="input" name="' . $field["name"] . '"  value="' . $field["value"] . '"><br>
        ';
    }
    echo '<input name="Submit" type="Submit" value="Submit"></form>';

    if (isset($_POST['Submit'])){
        foreach ($fields as &$field) {
            $field['value'] = $_POST[$field['name']];
        }
        unset($field);
        update_option('tw', $fields);
        echo  '<meta http-equiv="refresh" content="0" url=' . $_SERVER['REQUEST_URI'] .'">';
    };
}

/**
 * Створюємо тип запису twitt
 */
add_action('init', 'true_register_post_type_init'); // Использовать функцию только внутри хука init
function true_register_post_type_init()
{
    $labels = array(
        'name' => 'twitt',
        'singular_name' => 'twitt', // админ панель Добавить->Функцию
        'add_new' => 'Добавить twitt',
        'add_new_item' => 'Добавить twitt', // заголовок тега <title>
        'edit_item' => 'Редактировать twitt',
        'new_item' => 'Новый twitt',
        'all_items' => 'Все twitt',
        'view_item' => 'Просмотр twitt на сайте',
        'search_items' => 'Искать twitt',
        'not_found' => 'twitt не найдено.',
        'not_found_in_trash' => 'В корзине нет twitt.',
        'menu_name' => 'twitts' // ссылка в меню в админке
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_ui' => true, // показывать интерфейс в админке
        'has_archive' => true,
        //'menu_icon' => get_stylesheet_directory_uri() .'/img/function_icon.png', // иконка в меню
        'menu_position' => 4, // порядок в меню
        'supports' => array('title', 'editor', 'author')//, 'thumbnail'
    );
    register_post_type('twitt', $args);
    flush_rewrite_rules(false);
}

/**
 * Знаходимо номер (він же назва) останього запису
 * @return int
 */
function last_twitt()
{
    $args = array('numberposts' => 1, 'post_type' => 'twitt', 'order' => 'ASC', 'orderby' => 'title');
    if (0 === count(get_posts($args)))
        $ret = 0;
    else
        $ret = intval(get_posts($args)[0]->post_title);
    return $ret;
}

/**
 * Отримуємо твіти
 * @param $twitt_since_id int перший твіт
 * @param $twitt_count int кількість
 * @return array
 */
function get_twitts($twitt_since_id, $twitt_count)
{
    if (($twitt_since_id) <= 0) {
        $twitt_since_id_str = "";
    } else {
        $twitt_since_id_str = "&since_id=" . $twitt_since_id;
    }

    if (($twitt_count) <= 0) {
        $twitt_count_str = "";
    } else {
        $twitt_count_str = "&count=" . $twitt_count;
    }
    require_once('TwitterAPIExchange.php');

    $fields = get_option('tw');

    $settings = [];
    foreach ($fields as &$field) {
        $settings[$field['name']] = $field['value'];
    }

    $user_screen_name = $settings['twitter_user'];

    $url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
    $getfield = '?screen_name=' . $user_screen_name . $twitt_since_id_str . $twitt_count_str;
    $requestMethod = 'GET';

    $twitters = new TwitterAPIExchange($settings);

    $twitts = json_decode($twitters->setGetfield($getfield)
        ->buildOauth($url, $requestMethod)
        ->performRequest()
    );


    $return_twitts = [];
    foreach ($twitts as $twitt) {
        $twitt_id = $twitt->id;
        $return_twitts[] = ['screen_name' => $user_screen_name, "id" => $twitt_id];

    }
    return $return_twitts;
}

/**
 * Створюємо запис типу twitt, з отриманими твітами
 * @internal param array $twitts
 */
function save_twitt()
{
    $twitts = get_twitts(last_twitt() + 1, 0);
    foreach ($twitts as $twitt) {
        $twitt_id = $twitt["id"];
        if (is_null(get_page_by_title(strval($twitt_id), OBJECT, 'twitt'))) {
            $user_screen_name = $twitt["screen_name"];
            $twit_url = "https://twitter.com/" . $user_screen_name . "/status/" . $twitt_id;
            // Вставляем запись в базу данных
            $defaults = array(
                'post_status' => 'publish',
                'post_type' => 'twitt',
                'post_title' => $twitt_id,
                'post_content' => $twit_url,
                //'post_author'   => $user_ID,
                'ping_status' => get_option('default_ping_status'),
                'post_parent' => 0,
                'menu_order' => 0,
                'to_ping' => '',
                'pinged' => '',
                'post_password' => '',
                'guid' => '',
                'post_content_filtered' => '',
                'post_excerpt' => '',
                'import_id' => 0
            );
            $post_id = wp_insert_post($defaults);

        }
    }
}

/**
 * Добавляємо часові інтервали
 */
add_filter('cron_schedules', 'true_moi_interval');
function true_moi_interval($interval)
{
    $interval['every_3_min'] = array(
        'interval' => 180, // в одной минуте 60 секунд, в трёх минутах - 180
        'display' => 'Каждые три минуты' // отображаемое имя
    );

    return $interval;
}


/**
 * Заплановуємо отримувати нові твіти кожні три хвилини
 */


add_action('add_new_twitts', 'save_twitt');
if (!wp_next_scheduled('add_new_twitts'))
    wp_schedule_event(time(), 'every_3_min', 'add_new_twitts');

/**
 * Створюємо власні віджети
 */
class TwittWidget extends WP_Widget
{
    public function __construct()
    {
        parent::__construct("twitt_widget", __("Twitts", 'wtw'),
            array("description" => __("Widget show twitts from database", 'wtw')));
    }

    public function form($instance)
    {


        $title = "";
        $number = 1;
        // если instance не пустой, достанем значения
        if (!empty($instance)) {
            $title = $instance["title"];
            $number = $instance["number"];
        }

        $tableId = $this->get_field_id("title");
        $tableName = $this->get_field_name("title");
        echo '<label for="' . $tableId . '">' . __('Title', 'tw') . '</label><br>';
        echo '<input id="' . $tableId . '" type="text" name="' .
            $tableName . '" value="' . $title . '"><br>';

        $numberId = $this->get_field_id("number");
        $numberName = $this->get_field_name("number");
        echo '<label for="' . $numberId . '">' . __('Max number', 'tw') . '</label><br>';
        echo '<input id="' . $numberId . '" type="number"  min="1" name="' . $numberName .
            '" value="' . $number . '"><br>';

    }

    public function update($newInstance, $oldInstance)
    {
        $values = array();
        $values["title"] = htmlentities($newInstance["title"]);
        $values["number"] = htmlentities($newInstance["number"]);
        return $values;
    }

    public function widget($args, $instance)
    {
        echo $args['before_widget']
            . $args['before_title']
            . $instance['title']
            . $args['after_title'];
        $number = $instance['number'];
        $twitts = get_posts(array('post_type' => 'twitt', 'numberposts' => $number, 'order' => 'DESC', 'orderby' => 'title'));
        foreach ($twitts as $twitt) {
            echo(json_decode(wp_remote_get('https://publish.twitter.com/oembed?url=' . $twitt->post_content)["body"])->html);
            echo '<hr>';
        }
        echo $args['after_widget'];
    }
}

add_action("widgets_init", function () {
    register_widget("TwittWidget");
});


?>