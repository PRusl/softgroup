<h1 class="page-header">
    Page Heading
    <small>Secondary Text</small>
</h1>
<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
        <?php if( is_home() || is_tax() || is_category()  ){ ?>
            <hr>
            <h1>
                <a href="<?php the_permalink(); ?>">
                    <?php the_title(); ?>
                </a>
            </h1>
            <p class="lead">
                by <a href="index.php"><?php the_author(); ?></a>
            </p>
            <span class="glyphicon glyphicon-time"></span>
            <?php
            echo __('Posted on', 'sg');
            the_time(' F j, Y ');
            echo __('at', 'sg');
            the_time(' h:i A');
            ?>
            <hr>
            <?php the_post_thumbnail(
                array(900, 300),
                array('class' => 'img-responsive'),
                array('alt' => '')
            ) ?>
            <hr>
            <?php the_excerpt(); ?>
            <a class="btn btn-primary" href="<?php the_permalink(); ?>"><?= __('Read More', 'sg') ?>
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        <?php } else { ?>
            <h1><?php the_title() ?></h1>
            <?php the_content() ?>
        <?php } ?>
    <?php endwhile; ?>
<?php endif; ?>