<?php
/**
 * Підключаємо локалізацію шаблону
 */
load_theme_textdomain('sg', get_template_directory() . '/languages');


/**
 * Додаємо favico, charset, viewport
 */
function add_head_meta_tags()
{
    ?>
    <link rel="shortcut icon" href="<?php bloginfo("template_url"); ?>/favico.png" type="image/x-icon">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">

    <!-- Bootstrap Core CSS -->
    <link href="../css/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/blog-home.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="http://cdn.jsdelivr.net/zepto/1.0rc1/zepto.min.js" type="text/javascript"></script>
    <script src="social-share-urls-master/generator.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../social-share-urls-master/font.css">

    <?php
}

add_action('wp_head', 'add_head_meta_tags');


/**
 * Реєструємо місце для меню
 */

register_nav_menus(array(
    'top' => __('Top menu', 'sg'),
));


/**
 * Реєструємо сайдбари теми
 */
if (function_exists('register_sidebar')) {
    register_sidebars(1, array(
        'id' => 'right',
        'name' => __('Right sidebar', 'sg'),
        'description' => '',
        'before_widget' => '<div class="well">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));

    register_sidebars(1, array(
        'id' => 'bottom',
        'name' => __('Bottom sidebar', 'sg'),
        'description' => '',
        'before_widget' => '<div class="col-lg-12">',
        'after_widget' => '</div>',
        'before_title' => '<p>',
        'after_title' => '</p>'
    ));
}


/**
 * Підключаємо підтримку мініатюр
 */
if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(150, 150);
}
/**
 * Можемо добавити різні розміри для картинок
 */
if (function_exists('add_image_size')) {
    //add_image_size( 'photo', 148, 148, true );
}

/**
 * Створюємо власні віджети
 */
class TextWidget extends WP_Widget
{
    public function __construct()
    {
        parent::__construct("text_widget", "Simple Text Widget",
            array("description" => "A simple widget to show how WP Plugins work"));
    }

    public function form($instance)
    {
        $title = "";
        $taxonomy = null;
        $number = 0;
        // если instance не пустой, достанем значения
        if (!empty($instance)) {
            $title = $instance["title"];
            $taxonomy = $instance["taxonomy"];
            $number = $instance["number"];
        }

        $tableId = $this->get_field_id("title");
        $tableName = $this->get_field_name("title");
        echo '<label for="' . $tableId . '">' . __('Title', 'sg') . '</label><br>';
        echo '<input id="' . $tableId . '" type="text" name="' .
            $tableName . '" value="' . $title . '"><br>';

        $taxonomyId = $this->get_field_id("taxonomy");
        $taxonomyName = $this->get_field_name("taxonomy");
        echo '<label for="' . $taxonomyId . '">' . __('Taxonomies', 'sg') . '</label><br>';
        echo '<select id="' . $taxonomyId . '" name="' . $taxonomyName . '">';
        $taxonomies = get_taxonomies([], 'objects');
        foreach ($taxonomies as $taxonomyItem) {
            echo '<option value="' . $taxonomyItem->name . '"';
            if ($taxonomyItem->name == $taxonomy) {
                echo ' selected';
            }
            echo '> ' . $taxonomyItem->name . '</option>';
        }
        echo '</select> <br>';

        $numberId = $this->get_field_id("number");
        $numberName = $this->get_field_name("number");
        echo '<label for="' . $numberId . '">' . __('Max number', 'sg') . '</label><br>';
        echo '<input id="' . $numberId . '" type="number"  min="1" name="' . $numberName .
            '" value="' . $number . '"><br>';

    }

    public function update($newInstance, $oldInstance)
    {
        $values = array();
        $values["title"] = htmlentities($newInstance["title"]);
        $values["taxonomy"] = htmlentities($newInstance["taxonomy"]);
        $values["number"] = htmlentities($newInstance["number"]);
        return $values;
    }

    public function widget($args, $instance)
    {
        echo $args['before_widget']
            . $args['before_title']
            . $instance['title']
            . $args['after_title'];
        $terms = get_terms([
            'taxonomy' => [$instance['taxonomy']],
            'number' => $instance['number']
        ]);
        echo '<style>
                article {
                    -webkit-column-count:2;
                    -moz-column-count:2;
                    column-count:2;
                }
            </style>';
        echo '<article> ';
        foreach ($terms as $term) {
            echo '<a href="'
                . get_term_link($term)
                . '">'
                . $term->name
                . '</a>'
                . '<br>';
        }
        echo '</article>';
        echo $args['after_widget'];
    }

}

add_action("widgets_init", function () {
    register_widget("TextWidget");
});


/**
 * Реєструємо формати постів
 */
function add_post_formats()
{
    add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));
    add_theme_support('post-thumbnails');
    add_theme_support('custom-background');
    add_theme_support('custom-header');
    add_theme_support('custom-logo');
}

add_action('after_setup_theme', 'add_post_formats', 11);


/**
 * Замінюємо стандартне закінчення обрізаного тексту з [...] на ...
 */
function custom_excerpt_more($more)
{
    return ' ...';
}

add_filter('excerpt_more', 'custom_excerpt_more');


/**
 * Замінюємо стандартну довжину обрізаного тексту
 */
function custom_excerpt_length($length)
{
    return 200;
}

add_filter('excerpt_length', 'custom_excerpt_length', 999);


/**
 * Підключаємо javascript файли
 */
function add_theme_scripts()
{
    wp_enqueue_script("jquery");
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'));
    wp_enqueue_script('init', get_template_directory_uri() . '/js/init.js', array('jquery', 'bootstrap'));
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'));
    wp_enqueue_script('generator', get_template_directory_uri() . '/social-share-urls-master/generator.js', array('jquery'));
}

add_action('wp_enqueue_scripts', 'add_theme_scripts');


/**
 * Підключаємо css файли
 */
function add_theme_style()
{
    wp_enqueue_style('fonts', get_template_directory_uri() . '/fonts/stylesheet.css');
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style('style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('blog-home', get_template_directory_uri() . '/blog-home.css');
    wp_enqueue_style('soc', get_template_directory_uri() . 'social-share-urls-master/font.css');

}

add_action('wp_enqueue_scripts', 'add_theme_style');

//add_action('test', 'test');
//function test($a)
//{
//    return '1';
//}
//
//add_filter('test', 'test2');
//function test2($a)
//{
//    return '2';
//}


add_filter('the_content', 'change_url', 1000);
function change_url($content)
{
    include_once 'simple_html_dom.php';
    $localUrl = parse_url(get_site_url());
    $html = str_get_html($content);
    if (is_object($html)) {
        foreach ($html->find('a') as $element) {
            $farUrl = parse_url($element->href);
            if (isset($farUrl['host']) && $farUrl['host'] != $localUrl['host']) {
                $element->href = '/redirect?to=' . $element->href;
            }
        }
    }
    return $html;
}

add_action('init', 'redirectLinks');
function redirectLinks()
{
    if (strpos($_SERVER['REQUEST_URI'], '/redirect') === 0 &&
        (isset($_GET['to']))
    ) {
        wp_redirect($_GET['to']);
        exit;
    }
}

/*
 * Social-share
 */
add_filter('the_content', 'socialShare', 1000);
function socialShare($content)
{
    if (!is_home() && !is_tax() && !is_category()) {
        $content = $content . '
        <hr>
        <div id="share_target" class="social_links">
        <a class="soc Twitter"
           href="http://twitter.com/share?url=' . get_permalink() . '"
           title="Twitter"
           target="_blank">
               Twitter
        </a>

        <a class="soc facebook"
           href="http://www.facebook.com/sharer.php?p[url]=' . get_permalink() . '"
           title="Facebook"
           target="_blank">
               Facebook
        </a>

        <a class="soc vk"
           href="http://vk.com/share.php?url=' . get_permalink() . '"
           title="VKontakte"
           target="_blank">
               VK
        </a>
        
        </div>';
    }
    return $content;
}


class TaxQWidget extends WP_Widget
{
    public function __construct()
    {
        parent::__construct("tax_q_widget", "Last posts",
            array("description" => "Show last posts from taxonomies"));
    }

    public function form($instance)
    {
        $title = "";
        $taxonomy = null;
        $number_taxonomies = 1;
        $number_posts = 1;
        // если instance не пустой, достанем значения
        if (!empty($instance)) {
            $title = $instance["title"];
            $taxonomy = $instance["taxonomy"];
            $number_taxonomies = $instance["number_taxonomies"];
            $number_posts = $instance["number_posts"];
        }

        $tableId = $this->get_field_id("title");
        $tableName = $this->get_field_name("title");
        echo '<label for="' . $tableId . '">' . __('Title', 'sg') . '</label><br>';
        echo '<input id="' . $tableId . '" type="text" name="' .
            $tableName . '" value="' . $title . '"><br>';

        $taxonomyId = $this->get_field_id("taxonomy");
        $taxonomyName = $this->get_field_name("taxonomy");
        echo '<label for="' . $taxonomyId . '">' . __('Taxonomies', 'sg') . '</label><br>';
        echo '<select id="' . $taxonomyId . '" name="' . $taxonomyName . '">';
        $taxonomies = get_taxonomies([], 'objects');
        foreach ($taxonomies as $taxonomyItem) {
            echo '<option value="' . $taxonomyItem->name . '"';
            if ($taxonomyItem->name == $taxonomy) {
                echo ' selected';
            }
            echo '> ' . $taxonomyItem->name . '</option>';
        }
        echo '</select> <br>';

        $number_taxonomiesId = $this->get_field_id("number_taxonomies");
        $number_taxonomiesName = $this->get_field_name("number_taxonomies");
        echo '<label for="' . $number_taxonomiesId . '">' . __('Max number taxonomies', 'sg') . '</label><br>';
        echo '<input id="' . $number_taxonomiesId . '" type="number"  min="1" name="' . $number_taxonomiesName .
            '" value="' . $number_taxonomies . '"><br>';

        $number_postsId = $this->get_field_id("number_posts");
        $number_postsName = $this->get_field_name("number_posts");
        echo '<label for="' . $number_postsId . '">' . __('Max number posts', 'sg') . '</label><br>';
        echo '<input id="' . $number_postsId . '" type="number"  min="1" name="' . $number_postsName .
            '" value="' . $number_posts . '"><br>';

    }

    public function update($newInstance, $oldInstance)
    {
        $values = array();
        $values["title"] = htmlentities($newInstance["title"]);
        $values["taxonomy"] = htmlentities($newInstance["taxonomy"]);
        $values["number_taxonomies"] = htmlentities($newInstance["number_taxonomies"]);
        $values["number_posts"] = htmlentities($newInstance["number_posts"]);
        return $values;
    }

    public function widget($args, $instance)
    {
        echo $args['before_widget']
            . $args['before_title']
            . $instance['title']
            . $args['after_title'];
        $terms = get_terms([
            'taxonomy' => [$instance['taxonomy']],
            'number_taxonomies' => $instance['number_taxonomies']
        ]);
        echo '<style>
                article {
                    -webkit-column-count:2;
                    -moz-column-count:2;
                    column-count:2;
                }
            </style>';
        echo '<article> ';
        foreach ($terms as $term) {
            echo '<a href="'
                . get_term_link($term)
                . '">'
                . $term->name
                . '</a>'
                . '<br>';
        }
        echo '</article>';
        echo $args['after_widget'];
    }

}

add_action("widgets_init", function () {
    register_widget("TaxQWidget");
});
